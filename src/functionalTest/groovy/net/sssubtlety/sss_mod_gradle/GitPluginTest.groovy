package net.sssubtlety.sss_mod_gradle

import org.eclipse.jgit.api.Git
import spock.lang.Specification
import spock.lang.TempDir

import java.nio.file.Files

import static java.util.regex.Pattern.quote
import static net.sssubtlety.sss_mod_gradle.util.StringUtil.LINE_ENDING_REGEX

/*
TODO: test configuring with PublishingPluginExtension
*/

abstract class GitPluginTest extends Specification {
    protected static final def CURRENT_VERSION = "1.1.1"
    protected static final CHANGELOG_MD = 'CHANGELOG.md'
    protected static final String INITIAL_CHANGELOG_CONTENTS =
        """\
        - 1.1.0 (1 Jan. 2020):
            - Minor change 1
            - Minor change 2
        - 1.0.0 (1 Jan. 2020): Initial release
        """.stripIndent()

    protected void addEntry(String entry) {
        final def contents = Files.readString(changelogFile.toPath())
        changelogFile.write(entry + System.lineSeparator() + contents)
    }

    @TempDir File testProjectDir
    File propsFile
    File buildFile
    File changelogFile
    Git repo

    def setup() {
        propsFile = new File(testProjectDir as File, 'gradle.properties')
        propsFile.write('')

        changelogFile = new File(testProjectDir, CHANGELOG_MD)
        changelogFile << INITIAL_CHANGELOG_CONTENTS

        buildFile = new File(testProjectDir, 'build.gradle')
        buildFile <<
            """
            plugins {
                id 'net.sssubtlety.git'
            }
            """

        File gitIgnoreFile = new File(testProjectDir, '.gitignore')
        gitIgnoreFile.write('.gradle/')

        repo = Git.init().setDirectory(testProjectDir).call()
        repo.add().addFilepattern('.').call()
        repo.commit().setMessage('Initial commit').call()
    }

    protected void setPropAndCommit(String name, String value) {
        propsFile << "$name = $value"
        repo.add().addFilepattern(testProjectDir.toPath().relativize(propsFile.toPath()).toString()).call()
        repo.commit().setMessage("Set " + name).call()
    }
}
