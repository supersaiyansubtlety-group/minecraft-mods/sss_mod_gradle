package net.sssubtlety.sss_mod_gradle.commit_version

import net.sssubtlety.sss_mod_gradle.GitPluginTest
import org.gradle.testkit.runner.GradleRunner

abstract class CommitVersionTaskTest extends GitPluginTest {
    protected static final def COMMIT_VERSION = 'commitVersion'

    def runWithCustomVersion(String version) {
        GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withArguments(COMMIT_VERSION, "-PcustomVersion=$version")
            .withPluginClasspath()
            .build()
    }

    def runWithBumpVersion(String versionPart) {
        GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withArguments(COMMIT_VERSION, "-PbumpVersion=$versionPart")
            .withPluginClasspath()
            .build()
    }
}