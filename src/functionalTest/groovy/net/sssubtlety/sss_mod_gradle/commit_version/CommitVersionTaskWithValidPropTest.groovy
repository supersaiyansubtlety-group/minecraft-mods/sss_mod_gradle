package net.sssubtlety.sss_mod_gradle.commit_version

import net.sssubtlety.sss_mod_gradle.plugin.MainPlugin

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class CommitVersionTaskWithValidPropTest extends CommitVersionTaskTest {
    private static List<String> getExpectedPropFileLines(String version) {
        List.of("mod_version = $version".toString())
    }

    @Override
    def setup() {
        setPropAndCommit(MainPlugin.MOD_VERSION, CURRENT_VERSION)
        addEntry("Added some things")
    }

    def "with valid custom version"() {
        when:
        final def result = runWithCustomVersion(customVersion)

        then:
        result.task(":$COMMIT_VERSION").outcome == SUCCESS
        propsFile.readLines() == getExpectedPropFileLines(customVersion)

        where:
        customVersion   |_
        '2.0.0'         |_
        '1.2.0'         |_
        '1.1.2'         |_
    }

    def "with invalid custom version"() {
        when:
        runWithCustomVersion(customVersion)

        then:
        final RuntimeException e = thrown()
        e.message.contains "New version is not greater than current version; new: $customVersion; current: $CURRENT_VERSION"

        where:
        customVersion   |_
        '1.1.1'         |_
        '0.1.1'         |_
        '1.0.1'         |_
        '1.1.0'         |_
    }

    def "invalid customVersion"() {
        given:
        final def customVersion = '2'
        when:
        runWithCustomVersion(customVersion)

        then:
        final RuntimeException e = thrown()
        e.message.contains 'Could not parse new version'
    }

    def "with bumpVersion"() {
        when:
        final def result = runWithBumpVersion(bumpVersion)

        then:
        result.task(":$COMMIT_VERSION").outcome == SUCCESS
        propsFile.readLines() == getExpectedPropFileLines(expectedVersion)

        where:
        bumpVersion || expectedVersion
        'MAJOR'     || '2.0.0'
        'MINOR'     || '1.2.0'
        'PATCH'     || '1.1.2'
    }

    def "NONSENSE bumpVersion"() {
        given:
        final def bumpVersion = 'NONSENSE'
        when:
        runWithBumpVersion(bumpVersion)

        then:
        final RuntimeException e = thrown()
        e.message.contains('Invalid version part specified for bumpVersion')
    }
}
