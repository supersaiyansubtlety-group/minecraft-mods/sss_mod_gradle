package net.sssubtlety.sss_mod_gradle.commit_version

import static net.sssubtlety.sss_mod_gradle.plugin.MainPlugin.MOD_VERSION

class CommitVersionTaskWithInvalidPropTest extends CommitVersionTaskTest {
//    private static final def CURRENT_VERSION_MISSING = "Current version is missing or not a String"
    private static final String COULD_NOT_PARSE = 'Could not parse current version'
    public static final String MOD_VERSION_NULL = "Expected $MOD_VERSION to be String but was null"

    @Override
    def setup() {
        addEntry("Something new")
    }

    def "run with customVersion and invalid current version"() {
        given:
        final def newVersion = '1.1.2'
        setPropAndCommit(MOD_VERSION, "1")

        when:
        runWithCustomVersion(newVersion)

        then:
        final RuntimeException e = thrown()
        e.message.contains COULD_NOT_PARSE
    }

    def "run with customVersion and no mod_version prop"() {
        given:
        final def newVersion = '1.1.2'
        setPropAndCommit("version_mod", CURRENT_VERSION)

        when:
        runWithCustomVersion(newVersion)

        then:
        final RuntimeException e = thrown()
        e.message.contains MOD_VERSION_NULL
    }

    def "run with bumpVersion and invalid current version"() {
        given:
        setPropAndCommit(MOD_VERSION, "1")
        final def versionPart = 'PATCH'
        when:
        runWithBumpVersion(versionPart)

        then:
        final RuntimeException e = thrown()
        e.message.contains COULD_NOT_PARSE
    }

    def "run with bumpVersion and no mod_version prop"() {
        given:
        setPropAndCommit("version_mod", CURRENT_VERSION)
        final def versionPart = 'PATCH'

        when:
        runWithBumpVersion(versionPart)

        then:
        final RuntimeException e = thrown()
        e.message.contains MOD_VERSION_NULL
    }
}