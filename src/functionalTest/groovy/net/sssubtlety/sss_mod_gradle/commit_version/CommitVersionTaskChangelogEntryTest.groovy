package net.sssubtlety.sss_mod_gradle.commit_version

import net.sssubtlety.sss_mod_gradle.plugin.MainPlugin

import java.nio.file.Files

import static java.util.regex.Pattern.quote
import static net.sssubtlety.sss_mod_gradle.util.StringUtil.*
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class CommitVersionTaskChangelogEntryTest extends CommitVersionTaskTest {
    private static final String NEW_VERSION = "1.2.3"

    @Override
    def setup() {
        setPropAndCommit(MainPlugin.MOD_VERSION, CURRENT_VERSION)
    }

    def "single line entry"() {
        given:
        final def entry = "Exciting new feature"
        addEntry(entry)

        when:
        final def result = runWithCustomVersion(NEW_VERSION)

        then:
        result.task(":$COMMIT_VERSION").outcome == SUCCESS
        repo.status().call().isClean()
        assertExpectedFileContents(convertToRegex(' ' + entry))
    }

    def "multi-line entry"() {
        given:
        final def entry =
            """\
            Exciting new feature 1
            Exciting new feature 2""".stripIndent()
        addEntry(entry)

        when:
        final def result = runWithCustomVersion(NEW_VERSION)

        then:
        result.task(":$COMMIT_VERSION").outcome == SUCCESS
        repo.status().call().isClean()
        assertExpectedFileContents(LINE_ENDING_REGEX + convertToRegex(prefixLines(entry, '  ')))
    }

    def "multi-line un-trimmed entry"() {
        given:
        final def entry =
            """\
            
            Exciting new feature 1
            Exciting new feature 2
            """.stripIndent()
        addEntry(entry)

        when:
        final def result = runWithCustomVersion(NEW_VERSION)

        then:
        result.task(":$COMMIT_VERSION").outcome == SUCCESS
        repo.status().call().isClean()

        assertExpectedFileContents(LINE_ENDING_REGEX + convertToRegex(prefixLines(entry.trim(), '  ')))
    }

    def "entry beginning version prefix"() {
        given:
        final def entry = "- 1.2.3 (1 Jan. 2020): Exciting new feature"
        addEntry(entry)

        when:
        runWithCustomVersion(NEW_VERSION)

        then:
        RuntimeException e = thrown()
        e.message.contains "$CHANGELOG_MD entry begins with version prefix"
    }

    def "entry and non-entry changelog changes"() {
        given:
        changelogFile << "changed"
        final def entry = "Exciting new feature"
        addEntry(entry)

        when:
        runWithCustomVersion(NEW_VERSION)

        then:
        RuntimeException e = thrown()
        e.message.contains "$CHANGELOG_MD contains multiple changes"
    }

    def "only non-entry changelog changes"() {
        given:
        changelogFile << "changed"

        when:
        runWithCustomVersion(NEW_VERSION)

        then:
        RuntimeException e = thrown()
        e.message.contains "$CHANGELOG_MD does not start with changelog entry"
    }

    def "no changes" () {
        when:
        runWithCustomVersion(NEW_VERSION)

        then:
        RuntimeException e = thrown()
        e.message.contains "Needs new changelog entry at the top of $CHANGELOG_MD"
    }

    def "entry and non-changelog changes"() {
        given:
        final def entry = "Exciting new feature"
        addEntry(entry)
        propsFile << System.lineSeparator() + 'some_prop = some_val'

        when:
        runWithCustomVersion(NEW_VERSION)

        then:
        RuntimeException e = thrown()
        e.message.contains 'Non-changelog changes present'
    }

    def "only non-changelog changes"() {
        given:
        propsFile << System.lineSeparator() + 'some_prop = some_val'

        when:
        runWithCustomVersion(NEW_VERSION)

        then:
        RuntimeException e = thrown()
        e.message.contains 'Non-changelog changes present'
    }

    protected void assertExpectedFileContents(String entryRegex) {
        assert Files.readString(changelogFile.toPath()).matches(
            $/- ${quote(NEW_VERSION)} \(\d{1,2} [A-Z][a-z]{2}\.? 20\d{2}\):/$ + entryRegex + LINE_ENDING_REGEX +
                convertToRegex(INITIAL_CHANGELOG_CONTENTS)
        )
    }
}
