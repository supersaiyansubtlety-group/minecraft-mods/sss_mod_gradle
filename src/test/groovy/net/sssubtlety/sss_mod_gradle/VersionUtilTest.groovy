package net.sssubtlety.sss_mod_gradle

import spock.lang.Specification

import static net.sssubtlety.sss_mod_gradle.util.VersionUtil.*

class VersionUtilTest extends Specification {
    static final def CURRENT_VERSION = '1.1.1'

    def "bumpVersion with valid VersionPart"() {
        expect:
        bumpVersion(CURRENT_VERSION, versionPart) == expected

        where:
        versionPart         || expected
        VersionPart.MAJOR   || '2.0.0'
        VersionPart.MINOR   || '1.2.0'
        VersionPart.PATCH   || '1.1.2'
    }

    def "tryBumpVersion with current version with suffix"() {
        expect:
        bumpVersion(CURRENT_VERSION + '-a1', VersionPart.PATCH) == '1.1.2'
    }

    def "tryBumpVersion with invalid current version"() {
        given:
        final def currentVersion = '1'

        when:
         bumpVersion(currentVersion, VersionPart.PATCH)

        then:
        RuntimeException e = thrown()
        e.message.contains COULD_NOT_PARSE_CURRENT_VERSION_ + currentVersion
    }
}
