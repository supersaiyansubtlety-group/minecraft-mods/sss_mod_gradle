package net.sssubtlety.sss_mod_gradle

import spock.lang.Specification

import static net.sssubtlety.sss_mod_gradle.util.StringUtil.*

class StringUtilTest extends Specification {
    private static final String LER = LINE_ENDING_REGEX
    private static final String LS = System.lineSeparator()

    def "convertToRegex"() {
        expect:
        convertToRegex(input) == expected

        where:
        input                                   | expected
        "single line"                           | /\Qsingle line\E/
        "system${LS}line"                       | /\Qsystem\E${LER}\Qline\E/
        "old Mac\rline"                         | /\Qold Mac\E${LER}\Qline\E/
        "Linux\nline"                           | /\QLinux\E${LER}\Qline\E/
        "Windows\r\nline"                       | /\QWindows\E${LER}\Qline\E/
        "trailing line end\n"                   | /\Qtrailing line end\E${LER}/
        "double line\nwith trailing end\n"      | /\Qdouble line\E${LER}\Qwith trailing end\E${LER}/
        "consecutive\n\nline ends"              | /\Qconsecutive\E${LER}${LER}\Qline ends\E/
        "consecutive trailing line ends\n\n"    | /\Qconsecutive trailing line ends\E${LER}${LER}/
    }

    def "convertLineEndingsToRegex"() {
        expect:
         convertLineEndingsToRegex(input) == expected

        where:
        input                                   | expected
        "single line"                           | "single line"
        "system${LS}line"                       | "system${LER}line"
        "old Mac\rline"                         | "old Mac${LER}line"
        "Linux\nline"                           | "Linux${LER}line"
        "Windows\r\nline"                       | "Windows${LER}line"
        "trailing line end\n"                   | "trailing line end${LER}"
        "double line\nwith trailing end\n"      | "double line${LER}with trailing end${LER}"
        "consecutive\n\nline ends"              | "consecutive${LER}${LER}line ends"
        "consecutive trailing line ends\n\n"    | "consecutive trailing line ends${LER}${LER}"
    }

    def "prefixLines"() {
        expect:
        prefixLines(lines, prefix) == expected

        where:
        lines                                   | prefix || expected
        "single line"                           | '-'    || "-single line"
        "system${LS}line"                       | '-'    || "-system${LS}-line"
        "old Mac\rline"                         | '-'    || "-old Mac\r-line"
        "Linux\nline"                           | '-'    || "-Linux\n-line"
        "Windows\r\nline"                       | '-'    || "-Windows\r\n-line"
        "trailing line end\n"                   | '-'    || "-trailing line end\n"
        "double line\nwith trailing end\n"      | '-'    || "-double line\n-with trailing end\n"
        "consecutive\n\nline ends"              | '-'    || "-consecutive\n-\n-line ends"
        "consecutive trailing line ends\n\n"    | '-'    || "-consecutive trailing line ends\n-\n"
    }
}
