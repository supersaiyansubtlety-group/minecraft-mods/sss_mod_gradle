package net.sssubtlety.sss_mod_gradle.task;

import net.sssubtlety.sss_mod_gradle.util.Lazy;
import net.sssubtlety.sss_mod_gradle.util.VersionUtil;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.*;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.sssubtlety.sss_mod_gradle.plugin.GitPlugin.BUMP_VERSION;
import static net.sssubtlety.sss_mod_gradle.plugin.GitPlugin.CUSTOM_VERSION;
import static net.sssubtlety.sss_mod_gradle.util.StringUtil.convertLineEndingsToRegex;
import static net.sssubtlety.sss_mod_gradle.util.StringUtil.prefixLines;

public abstract class CommitVersionTask extends DefaultTask {
    private static final String PREFIX = "prefix";
    private static final String NEEDS_NEW_CHANGELOG_ENTRY = "Needs new changelog entry at the top of CHANGELOG.md";
    private static final String NON_CHANGELOG_CHANGES_PRESENT = "Non-changelog changes present";
    private static final String DIFF_LEADING_VERSION_PREFIX_ENTRY_REGEX =
        "^\\+- " + VersionUtil.VERSION_REGEX + " \\(\\d{1,2} [A-Z][a-z]{2}\\.? 20\\d\\d\\):";
    private static final DateFormatSymbols PUNCTUATED_SHORT_MONTH_DFS;

    static {
        var dateFormatSymbols = new DateFormatSymbols();
        final String[] shortMonths = dateFormatSymbols.getShortMonths();
        for (int i = 0; i < shortMonths.length; i++) {
            if (dateFormatSymbols.getMonths()[i].length() > shortMonths[i].length()) shortMonths[i] += ".";
        }
        dateFormatSymbols.setShortMonths(shortMonths);
        PUNCTUATED_SHORT_MONTH_DFS = dateFormatSymbols;
    }

    @Input
    public abstract Property<String> getVersionPropName();

    @Input
    public abstract Property<Boolean> getNewVersionSpecified();

    @Input
    public abstract Property<String> getVersion();

    @InputDirectory
    public abstract RegularFileProperty getRepoDir();

    @OutputFile
    public abstract RegularFileProperty getGradlePropsFile();

    @OutputFile
    public abstract RegularFileProperty getChangelogFile();

    @TaskAction
    public void updateVersionAndChangelogEntryThenCommit() {
        try (final var repo = Git.open(getRepoDir().get().getAsFile())) {
            final Lazy<String> changelogPath = new Lazy<>(() ->
                getPathStringRelativeToRepo(getChangelogFile())
            );

            final var rawChangelogEntry = tryGetChangelogEntry(repo, changelogPath);

            final String version = getVersion().get();
            writeVersion(version);

            final String commitMessage, changelogEntry;
            if (rawChangelogEntry.indexOf('\n') < 0) {
                // single line entry
                changelogEntry = " " + rawChangelogEntry;
                commitMessage = changelogEntry;
            } else {
                // multiline entry, add newlines to both strings, indent changelogEntry
                commitMessage = System.lineSeparator() + rawChangelogEntry;
                changelogEntry = System.lineSeparator() + prefixLines(rawChangelogEntry, "  ");
            }

            final String date = new SimpleDateFormat("d MMM yyyy", PUNCTUATED_SHORT_MONTH_DFS).format(new Date());
            final var versionAndDatePrefix = version + " (" + date + "):";

            writeChangelogEntry("- " + versionAndDatePrefix + changelogEntry, repo, changelogPath);
            addAndCommit(commitMessage, repo, changelogPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeVersion(String version) {
        if (!getNewVersionSpecified().get()) throw new GradleException(
            "Neither " + CUSTOM_VERSION + " nor " + BUMP_VERSION + " set, but one must be set"
        );
        String versionPropName = getVersionPropName().get();
        final Path gradlePropsPath = getGradlePropsFile().get().getAsFile().toPath();
        try {
            final var lines = Files.readAllLines(gradlePropsPath);
            final Pattern modVersionPropPattern = Pattern.compile("^\\s*(?<" + PREFIX + ">" + versionPropName + " ?= ?).*");
            final int size = lines.size();
            boolean foundVersionProp = false;
            for (int i = 0; i < size; i++) {
                final Matcher modVersionPropMatcher = modVersionPropPattern.matcher(lines.get(i));
                if (modVersionPropMatcher.find()) {
                    foundVersionProp = true;
                    lines.set(i, modVersionPropMatcher.group(PREFIX) + version);
                    break;
                }
            }
            if (!foundVersionProp) throw new GradleException(
                "Couldn't parse current " + versionPropName + " in " + gradlePropsPath.getFileName()
            );

            Files.write(gradlePropsPath, String.join(System.lineSeparator(), lines).getBytes());
        } catch (IOException e) {
            throw new GradleException("Error accessing " + gradlePropsPath.getFileName() + ": " + e.getMessage());
        }
    }

//    private void formatChangelogEntryAndCommit() {
//        try (final var repo = Git.open(getRepoDir().get().getAsFile())) {
//            final Lazy<String> changelogPath = new Lazy<>(() ->
//                getPathStringRelativeToRepo(getChangelogFile())
//            );
//            final var rawChangelogEntry = tryGetChangelogEntry(repo, changelogPath);
//            final String commitMessage, changelogEntry;
//            if (rawChangelogEntry.indexOf('\n') < 0) {
//                // single line entry
//                changelogEntry = " " + rawChangelogEntry;
//                commitMessage = changelogEntry;
//            } else {
//                // multiline entry, add newlines to both strings, indent changelogEntry
//                commitMessage = System.lineSeparator() + rawChangelogEntry;
//                changelogEntry = System.lineSeparator() + prefixLines(rawChangelogEntry, "  ");
//            }
//
//            final String date = new SimpleDateFormat("d MMM yyyy", PUNCTUATED_SHORT_MONTH_DFS).format(new Date());
//            final var versionAndDatePrefix = getVersion().get() + " (" + date + "):";
//
//            writeChangelogEntry("- " + versionAndDatePrefix + changelogEntry, repo, changelogPath);
//            addAndCommit(commitMessage, repo, changelogPath);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }

    @NotNull
    private String getPathStringRelativeToRepo(RegularFileProperty fileProp) {
        return getRepoDir().get().getAsFile().toPath().relativize(
            fileProp.get().getAsFile().toPath()
        ).toString();
    }

    private void writeChangelogEntry(String entry, Git repo, Lazy<String> changelogPath) {
        try {
            repo.checkout()
                .setName("HEAD")
                .addPath(changelogPath.get())
                .call();

            final var changelogFile = getChangelogFile().get().getAsFile();
            final String newContents = entry + System.lineSeparator() + Files.readString(changelogFile.toPath());
            Files.write(changelogFile.toPath(), newContents.getBytes());
        } catch (GitAPIException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void addAndCommit(String commitMessage, Git repo, Lazy<String> changelogPath) {
        try {
            repo.add()
                .addFilepattern(changelogPath.get())
                .addFilepattern(getPathStringRelativeToRepo(getGradlePropsFile()))
                .setUpdate(true)
                .call();

            repo.commit()
                .setMessage(commitMessage)
                .call();
        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
    }

    private String tryGetChangelogEntry(Git repo, Lazy<String> changelogPath) {
        try {
            final var status = repo.status().call();

            assertOnlyPathAltered(status, changelogPath);

            final var diffOut = new ByteArrayOutputStream();
            repo.diff()
                .setContextLines(0)
                .setPathFilter(PathFilter.create(Objects.requireNonNull(changelogPath.get())))
                .setOutputStream(diffOut)
                .call();

            final var diffStr = diffOut.toString();
            /*
            diff --git a/CHANGELOG.md b/CHANGELOG.md
            index 267264a..17a33be 100644
            --- a/CHANGELOG.md
            +++ b/CHANGELOG.md
            @@ -0,0 +1,3 @@
            +TEST

            */
            final Matcher diffOnlyFirstLineChangeMatcher = Pattern.compile(convertLineEndingsToRegex(
                """
                ^diff --git a/CHANGELOG\\.md b/CHANGELOG\\.md
                index [a-z0-9]{7}\\.\\.[a-z0-9]{7} \\d{6}
                --- a/CHANGELOG\\.md
                \\+\\+\\+ b/CHANGELOG\\.md
                @@ -0,0 \\+1(?:,\\d+)? @@
                ((?:\\+[^\\n]*\\n)*)\
                """
            )).matcher(diffStr);

            if (diffOnlyFirstLineChangeMatcher.find()) {
                if (diffOnlyFirstLineChangeMatcher.end() != diffStr.length())
                    throw new GradleException(changelogPath.get() + " contains multiple changes");

                final String diffEntryGroup = diffOnlyFirstLineChangeMatcher.group(1);
                if (Pattern.compile(DIFF_LEADING_VERSION_PREFIX_ENTRY_REGEX).matcher(diffEntryGroup).find())
                    throw new GradleException(changelogPath.get() + " entry begins with version prefix");

                return diffEntryGroup.replaceAll("(?m)^\\+", "").trim();
            } else throw new GradleException(changelogPath.get() + " does not start with changelog entry");
        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
    }

    private static void assertOnlyPathAltered(Status status, Lazy<String> relPathString) {
//        System.out.println("status:" +
//            "\nadded: " + status.getAdded() +
//            "\nremoved" + status.getRemoved() +
//            "\nmissing" + status.getMissing() +
//            "\nconflicting" + status.getConflicting() +
//            "\nuntracked" + status.getUntracked() +
//            "\nmodified" + status.getModified() +
//            "\nchanged" + status.getChanged()
//        );
        if (
            status.getAdded().isEmpty() &&
            status.getRemoved().isEmpty() &&
            status.getMissing().isEmpty() &&
            status.getConflicting().isEmpty() &&
            status.getUntracked().isEmpty() &&
            status.getModified().size() <= 1 &&
            status.getChanged().size() <= 1
        ) {
            boolean changelogChanged = false;

            if (!status.getModified().isEmpty()) {
                if (status.getModified().contains(relPathString.get())) changelogChanged = true;
                else throw new GradleException(NON_CHANGELOG_CHANGES_PRESENT);
            }

            if (!status.getChanged().isEmpty()) {
                if (status.getChanged().contains(relPathString.get())) changelogChanged = true;
                else throw new GradleException(NON_CHANGELOG_CHANGES_PRESENT);
            }

            if (!changelogChanged) throw new GradleException(NEEDS_NEW_CHANGELOG_ENTRY);
        } else throw new GradleException(NON_CHANGELOG_CHANGES_PRESENT);
    }

}
