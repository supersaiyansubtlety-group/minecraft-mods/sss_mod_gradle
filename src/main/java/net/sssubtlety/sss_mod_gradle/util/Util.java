package net.sssubtlety.sss_mod_gradle.util;

import org.gradle.api.GradleException;
import org.gradle.api.Project;
import org.jetbrains.annotations.NotNull;

public class Util {
    private Util() { }

    public static String getStringProp(@NotNull Project project, String prop) {
        final Object val = project.getProperties().get(prop);
        if (val instanceof String stringProp) return stringProp;
        else throw new GradleException(
            "Expected " + prop + " to be String but was " + (val == null ? "null" : val.getClass().toString())
        );
    }
}
