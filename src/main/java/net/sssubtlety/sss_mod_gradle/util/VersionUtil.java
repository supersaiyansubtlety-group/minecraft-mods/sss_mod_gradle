package net.sssubtlety.sss_mod_gradle.util;

import org.gradle.api.GradleException;

import java.util.regex.Pattern;

public final class VersionUtil {
    public static final String COULD_NOT_PARSE_CURRENT_VERSION_ = "Could not parse current version: ";

    private VersionUtil() { }
    public static final String EXTENSION = "extension";
    public static final String VERSION_REGEX =
        "(?<" + VersionPart.MAJOR + ">\\d+)\\." +
        "(?<" + VersionPart.MINOR + ">\\d+)\\." +
        "(?<" + VersionPart.PATCH + ">\\d+)" +
        "(?<" + EXTENSION + ">-\\w+)?";
    public static final Pattern VERSION_PATTERN = Pattern.compile("^" + VERSION_REGEX);

    public static boolean newVersionGreater(String newVersion, String oldVersion) {
        final var parsedNew = VERSION_PATTERN.matcher(newVersion);
        final var parsedOld = VERSION_PATTERN.matcher(oldVersion);

        if (parsedNew.find()) {
            if (parsedOld.find()) {
                final int newMajor = Integer.parseInt(parsedNew.group(VersionPart.MAJOR.toString()));
                final int oldMajor = Integer.parseInt(parsedOld.group(VersionPart.MAJOR.toString()));
                if (newMajor == oldMajor) {
                    final int newMinor = Integer.parseInt(parsedNew.group(VersionPart.MINOR.toString()));
                    final int oldMinor = Integer.parseInt(parsedOld.group(VersionPart.MINOR.toString()));
                    if (newMinor == oldMinor) {
                        final int newPatch = Integer.parseInt(parsedNew.group(VersionPart.PATCH.toString()));
                        final int oldPatch = Integer.parseInt(parsedOld.group(VersionPart.PATCH.toString()));
                        if (newPatch == oldPatch) return parsedNew.group(EXTENSION) != null;
                        else return newPatch > oldPatch;
                    } else return newMinor > oldMinor;
                } else return newMajor > oldMajor;
            } else throw new GradleException(COULD_NOT_PARSE_CURRENT_VERSION_ + oldVersion);
        } else throw new GradleException("Could not parse new version: " + newVersion);
    }

    public static String bumpVersion(String currentVersion, VersionPart versionPart) {
        final var parsedVersion = VERSION_PATTERN.matcher(currentVersion);
        if (parsedVersion.find()) {
            int major = Integer.parseInt(parsedVersion.group(VersionPart.MAJOR.toString()));
            int minor, patch;
            switch (versionPart) {
                case MAJOR -> { major++; minor = 0; patch = 0; }
                case MINOR -> {
                    minor = Integer.parseInt(parsedVersion.group(VersionPart.MINOR.toString()));
                    minor++; patch = 0;
                }
                case PATCH -> {
                    minor = Integer.parseInt(parsedVersion.group(VersionPart.MINOR.toString()));
                    patch = Integer.parseInt(parsedVersion.group(VersionPart.PATCH.toString()));
                    patch++;
                }
                default -> throw new GradleException("Unexpected VersionPart value: " + versionPart);
            }
            return String.join(".", String.valueOf(major), String.valueOf(minor), String.valueOf(patch));
        } else throw new GradleException(COULD_NOT_PARSE_CURRENT_VERSION_ + currentVersion);
    }

    public enum VersionPart {
        MAJOR,
        MINOR,
        PATCH
    }
}
