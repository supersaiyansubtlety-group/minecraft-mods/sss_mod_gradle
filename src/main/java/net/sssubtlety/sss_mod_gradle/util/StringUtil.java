package net.sssubtlety.sss_mod_gradle.util;

import java.util.Arrays;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class StringUtil {
    private StringUtil() { }

    @SuppressWarnings("RegExpUnnecessaryNonCapturingGroup")
    public static final String LINE_ENDING_REGEX = "(?:\\r\\n|\\r|\\n)";
    private static final Pattern MULTI_LINE_PATTERN = Pattern.compile("(?m)^.*?$" + LINE_ENDING_REGEX + "?");

    public static String convertToRegex(String literal) {
        return convertLineEndingsToRegex(literal, str -> str.isEmpty() ? str : Pattern.quote(str));
    }

    public static String convertLineEndingsToRegex(String regex) {
        return convertLineEndingsToRegex(regex, UnaryOperator.identity());
    }

    private static String convertLineEndingsToRegex(String string, UnaryOperator<String> mapper) {
        return Arrays.stream(string.split(LINE_ENDING_REGEX, -1)).map(mapper)
            .collect(Collectors.joining(LINE_ENDING_REGEX));
    }

    public static String prefixLines(String lines, String prefix) {
        final Matcher matcher = MULTI_LINE_PATTERN.matcher(lines);
        final StringBuilder prefixedBuilder = new StringBuilder();
        while (matcher.find()) prefixedBuilder.append(prefix).append(matcher.group());

        return prefixedBuilder.toString();
    }
}
