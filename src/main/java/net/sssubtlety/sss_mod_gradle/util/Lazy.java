package net.sssubtlety.sss_mod_gradle.util;

import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class Lazy <T> {
    private T value;
    private boolean initialized;
    private final Supplier<T> initializer;

    public Lazy(Supplier<T> initializer) {
        this.initialized = false;
        this.initializer = initializer;
    }

    public @Nullable T get() {
        if (!initialized)  {
            initialized = true;
            value = initializer.get();
        }
        return value;
    }
}
