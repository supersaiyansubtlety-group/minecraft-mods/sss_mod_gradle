package net.sssubtlety.sss_mod_gradle.plugin;

import net.sssubtlety.sss_mod_gradle.extension.GitPluginExtension;
import net.sssubtlety.sss_mod_gradle.extension.MainPluginExtension;
import net.sssubtlety.sss_mod_gradle.task.CommitVersionTask;
import net.sssubtlety.sss_mod_gradle.util.VersionUtil;
import org.gradle.api.GradleException;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.file.RegularFile;
import org.gradle.api.provider.Provider;
import org.jetbrains.annotations.NotNull;

import static net.sssubtlety.sss_mod_gradle.plugin.MainPlugin.MOD_VERSION;
import static net.sssubtlety.sss_mod_gradle.util.Util.getStringProp;

public class GitPlugin implements Plugin<Project> {
    public static final String CUSTOM_VERSION = "customVersion";
    public static final String BUMP_VERSION = "bumpVersion";
    public static final String EXT_NAME = "sssGit";

    @Override
    public void apply(@NotNull Project project) {
        final var ext = project.getExtensions().create(EXT_NAME, GitPluginExtension.class);
        if (project.getPluginManager().hasPlugin("net.sssubtlety.sss-mod-gradle")) {
            ext.getModVersionPropName().convention(
                project.getExtensions().getByType(MainPluginExtension.class).getModVersionPropName()
            );
        } else ext.getModVersionPropName().convention(MOD_VERSION);

        ext.getChangelogFile().convention(project.getLayout().getProjectDirectory().file("CHANGELOG.md"));
        project.setVersion(getVersion(project, ext));

        final Provider<RegularFile> gradlePropsRegularFile = project.provider(() ->
            project.getLayout().getProjectDirectory().file(project.GRADLE_PROPERTIES)
        );

//        final var tests = project.getTasks().withType(Test.class);
//        tests.configureEach(test -> test.mustRunAfter(writeNewVersionTask));

        final var commitVersionTask = project.getTasks().register(
            "commitVersion", CommitVersionTask.class
        );
        final var props = project.getProperties();
        commitVersionTask.configure(task -> {
            task.getVersionPropName().set(ext.getModVersionPropName());
            task.getNewVersionSpecified().set(
                props.get(CUSTOM_VERSION) instanceof String ||
                props.get(BUMP_VERSION) instanceof String
            );
            task.getGradlePropsFile().set(gradlePropsRegularFile);
            task.getVersion().set((String) project.getVersion());
//            task.dependsOn(tests);
            task.doNotTrackState("Otherwise 'Failed to create MD5 hash for file content'");
            task.getRepoDir().set(project.getProjectDir());
            task.getGradlePropsFile().set(gradlePropsRegularFile);
            task.getChangelogFile().set(ext.getChangelogFile());
        });
    }

    private static String getVersion(Project project, GitPluginExtension ext) {
        final var currentVersion = getStringProp(project, ext.getModVersionPropName().get());
        final var props = project.getProperties();
        final var customVersionObj = props.get(CUSTOM_VERSION);
        if (customVersionObj != null) {
            if (customVersionObj instanceof String customVersion) {
                if (VersionUtil.newVersionGreater(customVersion, currentVersion)) {
                    return customVersion;
                } else throw new GradleException(
                    "New version is not greater than current version; new: " +
                        customVersion + "; current: " + currentVersion
                );
            } else throw new GradleException(
                "Expected customVersion to be String but was " + customVersionObj.getClass()
            );
        } else {
            final Object bumpVersionObj = props.get(BUMP_VERSION);
            if (bumpVersionObj != null) {
                if (props.get(BUMP_VERSION) instanceof String bumpVersion) {
                    try {
                        return VersionUtil.bumpVersion(currentVersion, VersionUtil.VersionPart.valueOf(bumpVersion));
                    } catch (IllegalArgumentException e) {
                        throw new GradleException("Invalid version part specified for bumpVersion: " + bumpVersion);
                    }
                } else throw new GradleException(
                    "Expected customVersion to be String but was " + bumpVersionObj.getClass()
                );
            } else return currentVersion;
        }
    }
}
