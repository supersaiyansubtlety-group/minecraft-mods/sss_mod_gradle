package net.sssubtlety.sss_mod_gradle.plugin;

import net.sssubtlety.sss_mod_gradle.extension.MainPluginExtension;
import net.sssubtlety.sss_mod_gradle.util.Util;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.jetbrains.annotations.NotNull;

public class MainPlugin implements Plugin<Project> {
    public static final String EXT_NAME = "sss";
    public static final String MOD_VERSION = "mod_version";

    @Override
    public void apply(@NotNull Project project) {
        final var ext = project.getExtensions().create(EXT_NAME, MainPluginExtension.class);
        ext.getModName().convention(Util.getStringProp(project, "mod_name"));
        ext.getModVersionPropName().convention(MOD_VERSION);
        ext.getCurrentMcVersion().convention(Util.getStringProp(project, "mc_version"));
        ext.getMinMcVersionPropName().convention("min_mc_version");
        ext.getMcCompatVersionRange().convention(createMcCompatSemverRange(project, ext));

        // TODO: add getMcCompatVersionRange requirement to FMJ

        project.getPluginManager().apply(GitPlugin.class);
        project.setVersion(project.getVersion() + getVersionSuffix(project, ext));
    }

    private static Provider<String> createMcCompatSemverRange(@NotNull Project project, MainPluginExtension ext) {
        return project.provider(() -> {
            final String max = ext.getCurrentMcVersion().get();
            final String min = Util.getStringProp(project, ext.getMinMcVersionPropName().get());
            return ">=" + min + " <=" + max;
        });
    }

    private static String getVersionSuffix(@NotNull Project project, MainPluginExtension ext) {
        final String mcMax = ext.getCurrentMcVersion().get();
        final String mcMin = Util.getStringProp(project, ext.getMinMcVersionPropName().get());
        return "+MC" + (mcMin.equals(mcMax) ? mcMax : mcMin + "-" + mcMax);
    }
}
