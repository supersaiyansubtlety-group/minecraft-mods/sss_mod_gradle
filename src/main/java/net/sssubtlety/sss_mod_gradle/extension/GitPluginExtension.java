package net.sssubtlety.sss_mod_gradle.extension;

import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.Property;

public interface GitPluginExtension {
    Property<String> getModVersionPropName();
    RegularFileProperty getChangelogFile();
}
