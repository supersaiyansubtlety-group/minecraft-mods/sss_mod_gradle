package net.sssubtlety.sss_mod_gradle.extension;

import org.gradle.api.provider.Property;

public interface MainPluginExtension {
    Property<String> getModName();
    Property<String> getModVersionPropName();
    Property<String> getCurrentMcVersion();
    Property<String> getMinMcVersionPropName();
    Property<String> getMcCompatVersionRange();
}
